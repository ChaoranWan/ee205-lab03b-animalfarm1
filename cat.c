///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 03b - Animal Farm 1
///
/// @file cat.c
/// @version 1.0
///
/// Implements a simple database that manages cats
///
/// @author @todo yourName <@todo yourMail@hawaii.edu>
/// @brief  Lab 03b - AnimalFarm1 - EE 205 - Spr 2021
/// @date   @todo dd_mmm_yyyy
///////////////////////////////////////////////////////////////////////////////

#include <string.h>
#include <stdbool.h>
#include <stdio.h>

#include "cat.h"

// @todo declare an array of struct Cat, call it catDB and it should have at least MAX_SPECIES records in it
int main() {
   struct Cat CatDB[MAX_SPECIES];
   addAliceTheCat(1);
   printCat(1);

}

/// Add Alice to the Cat catabase at position i.
/// 
/// @param int i Where in the catDB array that Alice the cat will go
///
/// @note This terrible style... we'd never hardcode this data, but it gets us started.
void addAliceTheCat(int i) {
   CatDB[i].Name = "Alice";
   CatDB[i].Gender = FEMALE;
   CatDB[i].Breed = MAIN_COON;
   CatDB[i].isFixed = YES;
   CatDB[i].weight = 12.34;
   CatDB[i].collar1_color = BLACK;
   CatDB[i].collar2_color = RED;
   CatDB[i].license = 12345;
}



/// Print a cat from the database
/// 
/// @param int i Which cat in the database that should be printed
///
void printCat(int i) { 
   printf("Cat Name = [%s]\n", catDB[i].name );
   printf("    gender = [%s]\n", catDB[i].Gender );
   printf("    breed = [%s]\n", catDB[i].breed );
   printf("    fixed = [%s]\n", catDB[i].fixed );
   printf("    weight = [%d]\n", catDB[i].weight );
   printf("    collar color 1 = [%s]\n", colorName( catDB[i].collar1_color ));
   printf("    collar color 2 = [%s]\n", colorName( catDB[i].collar2_color ));
   printf("    license = [%d]\n", catDB[i].license );

}

